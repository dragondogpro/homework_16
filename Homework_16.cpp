#include <iostream>
#include <time.h>

int GetDay() 
{
    struct tm newTime;
    time_t now = time(0);
    localtime_s(&newTime, &now);
    return newTime.tm_mday;
}

int main()
{
    setlocale(0, "");

    const int sizeI = 10;
    const int sizeJ = 10;

    int array[sizeI][sizeJ];

    //������� 1. ������� ��������� ������ ����������� NxN � ��������� ��� 
    //����� �������, ����� ������� � ��������� i � j ��� ����� i + j.

    //���������� �������
    for (int i = 0; i < sizeI; i++) 
    {
        for (int j = 0; j < sizeJ; j++) 
        {
            array[i][j] = i + j;
        }
    }

    //����� �������
    for (int i = 0; i < sizeI; i++)
    {
        for (int j = 0; j < sizeJ; j++)
        {
            std::cout << array[i][j] << '\t';
        }
        std::cout << '\n';
    }

    
    //������� 2. ������� ����� ��������� � ������ �������, ������
    //������� ����� ������� ������� �������� ����� ��������� �� N.
    int sum = 0;
    int N = 5;
    int dayNow = GetDay();
    int modDay = dayNow % N;

    for (int j = 0; j < sizeJ; j++)
    {
        sum += array[modDay][j];
    }
    
    std::cout << "����� ��������� � " << modDay << " ������ �������: " << sum;
}
